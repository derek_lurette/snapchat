//
//  SnapsViewController.swift
//  Snapchat
//
//  Created by Derek Lurette on 2017-11-12.
//  Copyright © 2017 Derek Lurette. All rights reserved.
//

import UIKit
import Firebase

class SnapsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var snaps : [Snap] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        Database.database().reference().child("users").child((Auth.auth().currentUser!.uid)).child("snaps").observe(DataEventType.childAdded) { (snapshot) in
            
            let snap = Snap()
            snap.from = (snapshot.value as? NSDictionary)!["from"] as! String
            snap.imageURL = (snapshot.value as? NSDictionary)!["imageURL"] as! String
            snap.description = (snapshot.value as? NSDictionary)!["description"] as! String
            snap.key = (snapshot.key)
            snap.uuid = (snapshot.value as? NSDictionary)!["uuid"] as! String
            
            self.snaps.append(snap)
            self.tableView.reloadData()
            
        }
        
        Database.database().reference().child("users").child((Auth.auth().currentUser!.uid)).child("snaps").observe(DataEventType.childRemoved) { (snapshot) in
            
            for snap in self.snaps {
                var index = 0
                if snap.key == snapshot.key {
                    self.snaps.remove(at: index)
                }
                index += 1
            }
            
            self.tableView.reloadData()
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if snaps.count == 0 {
            return 1
        } else {
            return snaps.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        
        if snaps.count == 0 {
            cell.textLabel?.text = "You have no snaps! 🙁"
        } else {
            let snap = snaps[indexPath.row]
            cell.textLabel?.text = snap.from
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let snap = snaps[indexPath.row]
        performSegue(withIdentifier: "ViewSnapSegue", sender: snap)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ViewSnapSegue" {
            let nextVC = segue.destination as! ViewSnapsViewController
            nextVC.snap = sender as! Snap
        }
    }
    
    @IBAction func logoutTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
