//
//  ViewSnapsViewController.swift
//  Snapchat
//
//  Created by Derek Lurette on 2017-11-14.
//  Copyright © 2017 Derek Lurette. All rights reserved.
//

import UIKit
import SDWebImage
import Firebase

class ViewSnapsViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var descriptionTextLabel: UILabel!
    var snap = Snap()

    override func viewDidLoad() {
        super.viewDidLoad()
        descriptionTextLabel.text = snap.description
        imageView.sd_setImage(with: URL(string: snap.imageURL))

        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Database.database().reference().child("users").child((Auth.auth().currentUser!.uid)).child("snaps").child(snap.key).removeValue()
        Storage.storage().reference().child("images").child("\(snap.uuid).jpeg").delete { (error) in
            print("There was an error deleting the image from the storage!")
        }
    }
    
}
