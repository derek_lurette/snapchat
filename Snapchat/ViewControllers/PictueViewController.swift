//
//  PictueViewController.swift
//  Snapchat
//
//  Created by Derek Lurette on 2017-11-13.
//  Copyright © 2017 Derek Lurette. All rights reserved.
//

import UIKit
import Firebase

class PictueViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var pictureImageView: UIImageView!
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    var imagePicker = UIImagePickerController()
    var uuid = NSUUID().uuidString
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        nextButton.isEnabled = false
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func cameraTapped(_ sender: Any) {
        imagePicker.sourceType = .savedPhotosAlbum
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func nextTapped(_ sender: Any) {
        
        nextButton.isEnabled = false
        
        let imageFolder = Storage.storage().reference().child("images")
        
        let imageData = UIImageJPEGRepresentation(pictureImageView.image!, 0.1)!
        
        imageFolder.child("\(uuid).jpeg").putData(imageData, metadata: nil) { (metadata, error) in
            if error != nil {
                print("There was an error uploading the picture: \(error)")
            } else {
                self.performSegue(withIdentifier: "SelectUserSegue", sender: metadata?.downloadURL()?.absoluteString)
            }
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let nextVC = segue.destination as! SelectUserViewController
        nextVC.imageURL = sender as! String
        nextVC.desc = descriptionTextField.text!
        nextVC.uuid = uuid
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let picture = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        pictureImageView.image = picture
        imagePicker.dismiss(animated: true, completion: nil)
        pictureImageView.backgroundColor = UIColor.white
        nextButton.isEnabled = true
    }
}
