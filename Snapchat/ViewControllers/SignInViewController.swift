//
//  SignInViewController.swift
//  Snapchat
//
//  Created by Derek Lurette on 2017-11-12.
//  Copyright © 2017 Derek Lurette. All rights reserved.
//

import UIKit
import Firebase

class SignInViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func goTapped(_ sender: Any) {
        Auth.auth().signIn(withEmail: emailTextField.text!, password: passwordTextField.text!) { (user, error) in
            if error != nil {
                print("There was error signing in: \(error)")
                Auth.auth().createUser(withEmail: self.emailTextField.text!, password: self.passwordTextField.text!, completion: { (user, error) in
                    if error != nil {
                        print("There was error creating the user: \(error)")
                    } else {
                        // Created user successfully!
                        Database.database().reference().child("users").child(user!.uid).child("email").setValue(user!.email!)
                        print("User created successfully")
                        self.performSegue(withIdentifier: "SignInSegue", sender: nil)
                    }
                })
            } else {
                // Signed in successfully!
                print("Signed in successfully")
                self.performSegue(withIdentifier: "SignInSegue", sender: nil)
            }
        }
    }
}



