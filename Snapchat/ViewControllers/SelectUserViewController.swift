//
//  SelectUserViewController.swift
//  Snapchat
//
//  Created by Derek Lurette on 2017-11-13.
//  Copyright © 2017 Derek Lurette. All rights reserved.
//

import UIKit
import Firebase

class SelectUserViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var users : [User] = []
    var imageURL = ""
    var desc = ""
    var uuid = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        Database.database().reference().child("users").observe(DataEventType.childAdded) { (snapshot) in
            
            let user = User()
            user.email = (snapshot.value as? NSDictionary)!["email"] as! String
            user.uid = snapshot.key
            self.users.append(user)
            self.tableView.reloadData()
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let user = users[indexPath.row]
        let cell = UITableViewCell()
        cell.textLabel?.text = user.email
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let user = users[indexPath.row]
        
        let snaps = ["from": Auth.auth().currentUser!.email!, "description": desc, "imageURL": imageURL, "uuid": uuid]
        Database.database().reference().child("users").child(user.uid).child("snaps").childByAutoId().setValue(snaps)
        
        navigationController?.popToRootViewController(animated: true)
    }
    
}
