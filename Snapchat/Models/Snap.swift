//
//  Snap.swift
//  Snapchat
//
//  Created by Derek Lurette on 2017-11-14.
//  Copyright © 2017 Derek Lurette. All rights reserved.
//

import Foundation

class Snap {
    var imageURL = ""
    var from = ""
    var description = ""
    var key  = ""
    var uuid = ""
}
